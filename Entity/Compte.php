<?php
/**
 * Created by PhpStorm.
 * User: Anais PEZON
 * Date: 26/09/2018
 * Time: 16:21
 */

namespace App\Entity;

use App\Util\ConnAPI;
use App\Util\VerifFormat;
use App\Entity\Operation;

require_once "Util/ConnAPI.php";
require_once "Util/VerifFormat.php";
require_once "Entity/Operation.php";

class Compte
{
    /**
     * Compte constructor
     */
    public function __construct()
    {
        $listeJson = ConnAPI::getJson();

        $tabOpe = [];
        foreach ($listeJson->{'operations'} as $operation){
            $ope = new Operation($operation);
            $ope->ajouteChamps();
            $tabOpe[] = $ope;
        }

        $this->listeOperation = $tabOpe;
    }

    /**
     * Méthode d'affichage des propriétés du compte : le solde,
     * et les autres propriétés sous forme de tableau.
     */
    public function affichageCompte(){
        //var_dump($this->listeOperation);

       $this->listeOperation = $this->triParDate();

        if(sizeof($this->listeOperation) > 0 ){

            echo "<div><h4> Solde : " . $this->calculSolde() . " </h4></div>";

            echo '<tr class="tabOpe">
                <th>Montant : </th>
                <th>Devise : </th>
                <th>Libellé : </th>
                <th>Date : </th>
                <th>Rib : </th>
                <th>Recette : </th>
                <th>Dépense : </th>
            </tr>';

            foreach ($this->listeOperation as $element){
                echo '<tr class="tabOpe">';
                echo "<td>". $element->montant."</td>";
                echo "<td>". $element->devise."</td>";
                echo "<td>". $element->libelle."</td>";
                echo "<td>". $element->date."</td>";
                echo "<td>". $element->rib."</td>";
                echo "<td>". $element->recette."</td>";
                echo "<td>". $element->depense."</td>";
                echo "</tr>";
            } // fin foreach

        }else {
            echo "Aucun élément trouvé.";
        }

    }

    /**
     * @param $rib String.
     * Modifie la propriété de la calsse listeOperation pour ne selectionner que celles correspondant au paramètre rib..
     * Ordonnancement par date à faire.
     */
    public function rechercheRIB($rib){
        $listeTriee = [];
        foreach($this->listeOperation as $operation){
            if (trim($operation->rib) == trim($rib)){
                $listeTriee[] = $operation;
            }
        } // fin foreach

        $this->listeOperation = $listeTriee;
    }

    /**
     * @param $dateDebut String
     * @param $dateFin String
     *
     * Recherche les opérations selon les dates entrées :
     * Si que celle de début, on chercher à partie de cette date.
     * Si que celle de fin, on cherche avant cette date.
     * Si deux dates instanciées, on recherche entre les deux (après la fin et avant le début).
     * Vérifie aussi que date Debut < date Fin;
     */
    public function rechercheDate($dateDebut, $dateFin){

        //Listing après la date de début :
        if($dateDebut != null){
            $listeTriee = [];
            foreach ($this->listeOperation as $operation){

                $dateOpe = VerifFormat::verifDate($operation->date);
                $dateD = VerifFormat::verifDate($dateDebut);

                if($dateOpe['annee'] > $dateD['annee']){
                    $listeTriee[] = $operation;
                }elseif($dateOpe['annee'] == $dateD['annee']){
                    if($dateOpe['mois'] > $dateD['mois']){
                        $listeTriee[] = $operation;
                    }elseif($dateOpe['mois'] == $dateD['mois']){
                        if($dateOpe['jour'] > $dateD['jour'] or $dateOpe['jour'] == $dateD['jour']){
                            $listeTriee[] = $operation;
                        }// fin if des jours
                    } //fin if des mois
                }// fin if des annees
            } // fin foreach

            //var_dump($listeTriee);
            $this->listeOperation = $listeTriee;

        } // fin if

        //Listing avant la date de fin.
        if($dateFin != null){
            $listeTriee = [];
            foreach ($this->listeOperation as $operation){

                $dateOpe = VerifFormat::verifDate($operation->date);
                $dateF = VerifFormat::verifDate($dateFin);

                if($dateOpe['annee'] < $dateF['annee']){
                    $listeTriee[] = $operation;
                }elseif($dateOpe['annee'] == $dateF['annee']){
                    if($dateOpe['mois'] < $dateF['mois']){
                        $listeTriee[] = $operation;
                    }elseif($dateOpe['mois'] == $dateF['mois']){
                        if($dateOpe['jour'] < $dateF['jour'] or $dateOpe['jour'] == $dateF['jour']){
                            $listeTriee[] = $operation;
                        }// fin if des jours
                    } //fin if des mois
                }// fin if des annees
            } // fin foreach

            //var_dump($listeTriee);
            $this->listeOperation = $listeTriee;

        } // fin if

        //Si date de début > à date de fin :
        if($dateDebut != null and $dateFin != null){
           VerifFormat::dateSup($dateDebut, $dateFin);
        }

    }

    /**
     * @return int
     * Calcule et retourne le solde selon le total des recette - le total des dépense de la liste d'Operation en cours.
     */
    public function calculSolde(){
        $totalRcette = 0;
        $totalDepense = 0;

        foreach ($this->listeOperation as $operation){
            $totalRcette += $operation->recette;
            $totalDepense += $operation->depense;
        }

        return $totalRcette - $totalDepense;
    }

    /**
     * @return array
     *
     * Cette fonction trie la liste des opération de la classe par date par ordre décroissant
     * et renvoie la liste triée.
     */
    public function triParDate(){

        $tab = [];
        foreach ($this->listeOperation as $operation) {
            $tab[] = $operation;
        }

        $taille = count($tab);
        for ($i = 0; $i < $taille; $i++) {
            for ($j = 0; $j < $taille - 1 - $i; $j++) {
                $dateJ1 = VerifFormat::dateForm($tab[$j]->date);
                $dateJ2 = VerifFormat::dateForm($tab[$j + 1]->date);
                if ($dateJ1 < $dateJ2) {
                    $temp = $tab[$j + 1];
                    $tab[$j + 1] = $tab[$j];
                    $tab[$j] = $temp;
                } // fin if
            }// fin for J
        }//fin for i

        return $tab;
    }

}