<?php
/**
 * Created by PhpStorm.
 * User: Anaïs PEZON
 * Date: 26/09/2018
 * Time: 14:43
 */

namespace App\Entity;

class Operation
{

    /**
     * Operation.class constructor.
     * @param null $data
     */
    public function __construct($data = null){
        if(!empty($data))
            $this->hydrate($data);
    }

    /**
     * @param $data array d'objets importé du json
     * Instancie le constructeur en fonction d'un tableau de paramètre.
     */
    private function hydrate($data){
        $this->rib = $data->{"RIB"};
        $this->date = $data->{"Date"};
        $this->libelle = $data->{"Libelle"};
        $this->montant = $data->{"Montant"};
        $this->devise = $data->{"Devise"};
    }


    /**
     * Ajoute les champs recette et dépense selon le montant, transformé en "float".
     */
    public function ajouteChamps(){
        $nombre = floatval(str_replace(",", ".", $this->montant ) );
        //var_dump($nombre);
        if($nombre > 0){
            $this->recette = $nombre;
            $this->depense = 0;
        }
        else{
            $this->recette = 0;
            $this->depense = $nombre;
        }
    }




}