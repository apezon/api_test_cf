<?php
/**
 * Created by PhpStorm.
 * User: Anais PEZON
 * Date: 30/09/2018
 * Time: 14:18
 */

use App\Entity\Compte;
require_once "../Entity/Compte.php";
require_once "../Util/ConnAPI.php";
require_once "../Entity/Operation.php";

/*
$time = strtotime('28/03/2017');

$newformat = date('Y-m-d',$time);

echo $newformat;
// 2003-10-16
*/

list($jour, $mois, $annee) = sscanf("28/03/2017","%d/%d/%d");
echo $jour. " ". $mois . " ". $annee;


$compte = new Compte();

foreach ($compte->listeOperation as $operation){
    list($jour, $mois, $annee) = sscanf($operation->date,"%d/%d/%d");
    echo $jour. " ". $mois . " ". $annee . "<br>";
}

