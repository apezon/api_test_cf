<?php
/**
 * Created by PhpStorm.
 * User: Anais PEZON
 * Date: 26/09/2018
 * Time: 14:52
 */

namespace App\Util;

class ConnAPI
{
    public function __construct(){}

    /**
     * @return mixed
     */
    public static function getJson(){
       $resultat = file_get_contents("https://agrcf.lib.id/exercice@dev/");
       $json = json_decode($resultat);

       //var_dump($json);

       return $json;
    }

}

