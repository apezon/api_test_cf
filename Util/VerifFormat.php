<?php
/**
 * Created by PhpStorm.
 * User: Anais PEZON
 * Date: 27/09/2018
 * Time: 16:43
 */

namespace App\Util;


class VerifFormat
{
    /**
     * @param $rib String
     * @return bool
     *
     * Vérifie que le RIB fait bien 23 caractères.
     */
    public static function verifRib($rib){
        //echo "Taille de choixRib : " . strlen($rib) . "<br>";
        if(strlen($rib) == 23)
            return true;
        else return false;
    }

    /**
     * @param $date String
     * @return array
     *
     * Vérifie le format de la date YYYY-MM-DD (du champ date) ou DD/MM/YYYY (du json)
     * et renvoie un tableau contenant chacune des valeurs jour, mois et année.
     */
    public static function verifDate($date){
        if(strpos($date, '/') !== false ){
            list($jour, $mois, $annee) = sscanf($date,"%d/%d/%d");
            $dateTab = array('jour' => $jour, 'mois' =>  $mois, 'annee' => $annee);
            //echo $jour. " ". $mois . " ". $annee . "<br>";
            return $dateTab;
        }else {
            list($annee, $mois, $jour) = sscanf($date,"%d-%d-%d");
            $dateTab = array('jour' => $jour, 'mois' =>  $mois, 'annee' => $annee);
            //echo $jour. " ". $mois . " ". $annee . "<br>";
            return $dateTab;
        }
    }

    /**
     * @param $dateDebut String
     * @param $dateFin String
     * Vérifie que la date indiquée comme celle de début est bien inférieure ou égale à celle de fin.
     * Renvoie un message d'erreur dans le cas contraire.
     */
    public static function dateSup($dateDebut, $dateFin){
        $dateD = VerifFormat::verifDate($dateDebut);
        $dateF = VerifFormat::verifDate($dateFin);
        $dateSup = false;

        if($dateD['annee'] > $dateF['annee']){
            $dateSup = true;
        }elseif($dateD['annee'] == $dateF['annee']){
            if($dateD['mois'] > $dateF['mois']){
                $dateSup = true;
            }elseif($dateD['mois'] == $dateF['mois']){
                if($dateD['jour'] > $dateF['jour']){
                    $dateSup = true;
                }// fin if des jours
            } //fin if des mois
        }// fin if des annees

        if($dateSup == true) echo '<section class="erreur"> La date de début doit être inférieure à la date de fin ! </section>';
    }

    /**
     * @param $date
     * @return string
     *
     * Cette fonction prend une date, et la formate sous la forme d'une chaine de caracrère YYYYMMDD.
     */
    public static function dateForm($date){
        $dateForm = VerifFormat::verifDate($date);
        $annee = $dateForm['annee'];
        $mois = (strlen(strval($dateForm['mois'])) < 2)? '0'.$dateForm['mois'] : $dateForm['mois'];
        $jour = (strlen(strval($dateForm['jour'])) < 2)? '0'.$dateForm['jour'] : $dateForm['jour'];

        return $annee.$mois.$jour;
    }


}