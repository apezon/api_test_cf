<?php
/**
 * Created by PhpStorm.
 * User: Anais PEZON
 * Date: 26/09/2018
 * Time: 17:18
 */

use App\Entity\Compte;
use App\Util\VerifFormat;

require_once "Views/header.php";
require_once "Entity/Compte.php";
require_once "Util/VerifFormat.php";


try{
    $monCompte = new Compte();

    if(isset($_POST["dateDebut"]) and isset($_POST["dateFin"])){
        //echo $_POST["dateDebut"] . "<br>";
        //echo $_POST["dateFin"] . "<br>";
        $monCompte->rechercheDate($_POST["dateDebut"], $_POST["dateFin"]);
    } else if(isset($_POST["dateDebut"]) and !isset($_POST["dateFin"])){
        //echo $_POST["dateDebut"] . "<br>";
        $monCompte->rechercheDate($_POST["dateDebut"], null);
    } else if(!isset($_POST["dateDebut"]) and isset($_POST["dateFin"])) {
        //echo $_POST["dateFin"] . "<br>";
        $monCompte->rechercheDate(null, $_POST["dateFin"]);
    }

    if(isset($_POST["choixRib"])){
        //var_dump( $_POST["choixRib"]);
        if(VerifFormat::verifRib($_POST["choixRib"]) == true){
            //var_dump( $_POST["choixRib"]);
            $monCompte->rechercheRIB($_POST["choixRib"]);
        }
        elseif(($_POST["choixRib"]) != null and VerifFormat::verifRib($_POST["choixRib"]) == false){
            echo "Le RIB inséré n'est pas valide, ce champ doit contenir 23 caractères";
        }

    }
}catch (Exception $e) {
    echo "Erreur :  " . $e->getMessage() . "<br>";
}

?>

<section id="main">
    <h2>Affichage des opérations : </h2>

    <div id="filtre">
        <form action="main.php" method="post">
            <label for="dateDebut">Choisissez la date de début :</label>
            <input type="date" name="dateDebut">

            <label for="dateFin">Choisissez la date de fin :</label>
            <input type="date" name="dateFin">
            <br>
            <label for="choixRib">Insérez le rib voulu : </label>
            <input type="text" name="choixRib" size="23" maxlength="23" minlength="23">
            <br>
            <input type="submit" value="Rechercher">
            <!-- Insérer boutond e reset de recherche-->
        </form>
    </div> <!--Fin div #filtre -->

    <div id="affichage">
        <table>
            <?php
                echo $monCompte->affichageCompte();
            ?>
        </table>
    </div> <!--Fin div #affichage -->

</section>


<?php
require_once "Views/footer.php";